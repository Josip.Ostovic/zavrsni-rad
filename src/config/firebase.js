
import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";
import {getStorage} from "firebase/storage";
import {getAuth,GoogleAuthProvider} from "firebase/auth";
const firebaseConfig = {
  apiKey: "AIzaSyDsrOLINdMGYLio_pX4RJkZ8HUI04yij7s",
  authDomain: "zavrsni-afb52.firebaseapp.com",
  projectId: "zavrsni-afb52",
  storageBucket: "zavrsni-afb52.appspot.com",
  messagingSenderId: "811772655746",
  appId: "1:811772655746:web:1bf482c741f9088c149c9b",
  measurementId: "G-PJP5K2GGVV"
};


const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const storage = getStorage(app);
export const googleProvider = new GoogleAuthProvider();
