import { useNavigate,useLocation } from 'react-router-dom';
import PICTURES from '../colors/pictures.json'
import { useState,useEffect } from 'react';
import {db} from '../config/firebase';
import {addDoc, collection,getDocs} from 'firebase/firestore';


function Home(){
    
    const [selectedOption,setSelectedOption] = useState(null);
    const [currentIndex,setCurrentIndex] = useState(0);
    {/* const [textColor,setTextColor] = useState("blue"); */}
    const ratingRef = collection(db,"colors");
    const navigate = useNavigate();
    const [averageRatings, setAverageRatings] = useState({});
    const location = useLocation();
    const { nickname } = location.state || {};

    const handleClickNext = async() =>{
        if(selectedOption === null){
          console.log("Please select a rating before proceeding");
          return;
        }
        setCurrentIndex(currentIndex + 1);
        try {
            await addDoc(ratingRef, {
                Rback: `${PICTURES[currentIndex].Rback}`,
                Gback: `${PICTURES[currentIndex].Gback}`,
                Bback: `${PICTURES[currentIndex].Bback}`,
                Rtext: `${PICTURES[currentIndex].Rtext}`,
                Gtext: `${PICTURES[currentIndex].Gtext}`,
                Btext: `${PICTURES[currentIndex].Btext}`,
                rating: selectedOption,
                id: PICTURES[currentIndex].id,
                nickname: nickname
            });

            const querySnapshot = await getDocs(ratingRef);
            const ratingsByImage = {};

            querySnapshot.forEach((doc) => {
                const data = doc.data();
                if (data.id && data.rating) {
                    const imageId = data.id;
                    const rating = parseInt(data.rating);

                    if (!ratingsByImage[imageId]) {
                        ratingsByImage[imageId] = {
                            totalRating: rating,
                            count: 1,
                        };
                    } else {
                        ratingsByImage[imageId].totalRating += rating;
                        ratingsByImage[imageId].count++;
                    }
                }
            });

            const imageIds = Object.keys(ratingsByImage);
            const newAverageRatings = {};

            imageIds.forEach((imageId) => {
                const totalRating = ratingsByImage[imageId].totalRating;
                const count = ratingsByImage[imageId].count;
                const averageRating = totalRating / count;
                newAverageRatings[imageId] = averageRating.toFixed(2);
            });

            setAverageRatings(newAverageRatings);
        } catch (err) {
            console.error(err);
        }

        setSelectedOption(null);
      };

   {/* useEffect(() =>{
      const textVal = '#' + Math.floor(Math.random()*(256*256*256)).toString(16)
      setTextColor(textVal)

    },[])
  */}

    const handleNicknameSubmit = () =>{
      
    }

    
    return(
        <div className='container'>
         {PICTURES[currentIndex] && (
          <div>
            <div className='picture-container' 
              style={{background:`rgb(${PICTURES[currentIndex].Rback},
              ${PICTURES[currentIndex].Gback},
              ${PICTURES[currentIndex].Bback})`,
              height:"200px",width:"800px"}}>
              <div className='text-container' 
                style={{color:`rgb(${PICTURES[currentIndex].Rtext},
                ${PICTURES[currentIndex].Gtext},
                ${PICTURES[currentIndex].Btext})`,
                textAlign:'center'}}>
                Ovo je neki tekst
              </div>
            </div>
          </div>
         )}
         {/*{currentIndex < PICTURES.length && (
          <>
          <input type='checkbox' checked={selectedOption === "1"} onChange={() => setSelectedOption("1")}/>
          <label>1</label>
          <input type='checkbox' checked={selectedOption === "2"} onChange={() => setSelectedOption("2")}/>
          <label>2</label>
          <input type='checkbox' checked={selectedOption === "3"} onChange={() => setSelectedOption("3")}/>
          <label>3</label>
          <input type='checkbox' checked={selectedOption === "4"} onChange={() => setSelectedOption("4")}/>
          <label>4</label>
          <button onClick={handleClickNext}>
            Slijedeci element
          </button>
          </>
         )}*/}

        {currentIndex < PICTURES.length ? (<div className='checkbox'>
          <>
          <input type='checkbox' checked={selectedOption === "1"} 
            onChange={() => setSelectedOption("1")}/>
          <label className='label-checkbox'>1</label>
          <input type='checkbox' checked={selectedOption === "2"} 
            onChange={() => setSelectedOption("2")}/>
          <label className='label-checkbox'>2</label>
          <input type='checkbox' checked={selectedOption === "3"} 
            onChange={() => setSelectedOption("3")}/>
          <label className='label-checkbox'>3</label>
          <input type='checkbox' checked={selectedOption === "4"} 
            onChange={() => setSelectedOption("4")}/>
          <label className='label-checkbox'>4</label>
          <input type='checkbox' checked={selectedOption === "5"} 
            onChange={() => setSelectedOption("5")}/>
          <label className='label-checkbox'>5</label>
          <button onClick={handleClickNext} className='button-slijedeci'>
            Slijedeci element
          </button>
          </>
          </div>)
          :
          (  
            <div className='table-container'>
              <table className='rating-table'>
                <thead>
                  <tr>
                    <th>Image ID</th>
                    <th>Average Rating</th>
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(averageRatings).map((imageId) => (
                  <tr key={imageId}>
                    <td>{imageId}</td>
                    <td>{averageRatings[imageId]}</td>
                  </tr>
                ))}
                </tbody>
                </table>
                <button className='back-home-btn' onClick={() => navigate('/')}>
                  Back to Home
                </button>
              </div>
            )}
          </div>
    );
    

}
export default Home;