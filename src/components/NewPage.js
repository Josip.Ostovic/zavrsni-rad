import { useState } from "react";
import { useNavigate } from "react-router-dom";

function NewPage(){
    const navigate = useNavigate();
    const [nickname,setNickname] = useState('');
    const [isNicknameEntered,setIsEnteredNickname] = useState(false);


    const handleChangeNickname = (e) =>{
        setNickname(e.target.value);
        setIsEnteredNickname(e.target.value.trim() !== '');
    }

    const handleNicknameSubmit = () =>{
        if(isNicknameEntered){
            navigate('/home',{state:{nickname:nickname}});
        }
    }

    return <div>
        <input
            type="text"
            value={nickname}
            onChange={handleChangeNickname}
            placeholder="Enter your nickname"
            className="nickname-input"
        />
         <button onClick={handleNicknameSubmit} 
         disabled={!isNicknameEntered} 
         className="button-submit">Submit Nickname</button>
    </div>
}
export default NewPage;