function Employee(props){
    return(
        <>
        <h3>Employee {props.name}</h3>
        <p>{props.role}</p>
        <img src={props.img} alt="logo"/>
        </>
    );
}
export default Employee;