import Auth from './components/auth';
import './App.css';
import { useEffect,useState } from 'react';
import {db,auth,storage} from "./config/firebase";
import "./index.css";
import Employee from './components/Employee';
import IMAGES from './colors/images';
import ColorGenerator from './colors/ColorGenerator';
import { collection,addDoc } from 'firebase/firestore';
import { BrowserRouter as Router,Routes,Route, useNavigate, Link } from 'react-router-dom';
import NewPage from "./components/NewPage";
import ErrorPage from './components/ErrorPage';
import Home from './components/Home';


function App() {
  const [isChecked,setNewIsChecked] = useState(false);
  const [role,setRole] = useState("dev");
  const colorsCollectionRef = collection(db,"colors");

  return (
      <div className="App">
      <Router>
        <Routes>
          <Route path='/home' element={<Home/>}></Route>
          <Route path='*' element={<ErrorPage/>}></Route>
          <Route path='/' element={<NewPage/>}></Route>
        </Routes>
      </Router>
        
      {/*{
      employees.map((employee) =>{
      return(    
        <img src={require(employee.imgSrc)} alt='logo'/>
      );
      })}
      <input type='text' onChange={(e) => {
          setRole(e.target.value)}
        }
      />
      */}

      {/*<Employee name="Josip" role="Intern" img={employees.imgSrc}/>
      <Employee name="Mislav" />
      <Employee name="Patrik" role={role}/>
    */}
    {/*   <Auth />
        <img src={IMAGES.imgOne} alt='logo' />
        <img src={IMAGES.imgTwo} alt='logo' />
        
        <input type="checkbox" checked={isChecked} onChange={(e) => setNewIsChecked(e.target.checked)} className='checkboxes'/>
        <label>1</label>
        <input type="checkbox" className='checkboxes'/>
        <label>2</label>
        <input type="checkbox" className='checkboxes'/>
        <label>3</label>
        <input type="checkbox" className='checkboxes'/>
        <label>4</label>
        <input type="checkbox" className='checkboxes'/>
        <label>5</label>
        <button className='btn'>Spremi i idi dalje</button>
        <p>1-jako loša čitljivost<br/>
        5-odlična čitljivost</p>
        
        { PICTURES && PICTURES.map((picture) =>{
          return(
            <div key={picture.id}> 
                <img src={picture.imgSrc} alt="logo" />
            </div>    
          )
          
        })}
        
        {PICTURES && PICTURES.map((picture) => {
          return(
            <div key={picture.id}>
              <div style={{backgroundColor: `rgb(${picture.Rback},${picture.Gback},${picture.Bback})`,height:"100px",width:"800px",textAlign:'center'}}>
                {picture.Rback},{picture.Gback},{picture.Bback}
              </div>
              <input type="checkbox"></input>
              <label>1</label>
              <input type="checkbox"></input>
              <label>2</label>
              <input type="checkbox"></input>
              <label>3</label>
              <input type="checkbox"></input>
              <label>4</label>
              <input type="checkbox"></input>
              <label>5</label>
              <button>Submit Color</button>
            </div>
          );
        })}
        
      */}
      </div>
  );
}

export default App;
