import { useEffect, useState } from "react";
import PICTURES from "./pictures.json"
const ColorGenerator= () =>{
    const [backgroundColor,setBackgroundColor] = useState("");
    const [textColor,setTextColor] = useState("blue");
    useEffect(() =>{
        const val = '#' + Math.floor(Math.random()*(256*256*256)).toString(16)
        setBackgroundColor(val)
    },[])
    useEffect(() =>{
        const textVal = '#' + Math.floor(Math.random()*(256*256*256)).toString(16)
        setTextColor(textVal)
    },[])
    return(
        <div>
            <div style={{background:`${backgroundColor}`,height:"800px",width:"600px",margin:"100px"}}> 
                <div style={{color:`${textColor}`}}>
                    ovo je neki tekst
                    <h1>{textColor}</h1>
                    
                </div>
                <h1>{backgroundColor}</h1>
                  
            </div>
            
            
        </div>
    );
}
export default ColorGenerator;